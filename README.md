### Mimecast Server

Pull the dependancies

```
./gradlew cleanIdea idea
```

Launch the Mimecast server running the following commands:

```
./gradlew build
java -jar build/libs/mimecast-server-1.0.0.jar --server.port=8081
```

Then test we have launched correctly
```
curl localhost:8081

> Greetings from Spring Boot!
```

Or another entry
```
curl -d '{"message": "Youre a winner! Call 07935123898 to claim your FREE PRIZE NOW!!!!"}' -H "Content-Type: application/json" -X POST localhost:8081/api/spam/analyse

```