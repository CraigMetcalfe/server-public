package com.mimecast.server.services;

import com.mimecast.server.data.dao.MessageQueryOptions;
import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.models.SmsMessage;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

public interface SmsMessageProcessor
{
    List<SmsMessage> getMessages(MessageQueryOptions options);

    Pair<Long, Long> countHamSpam();

    Map<MessageCategory, Long> countCategories();
}
