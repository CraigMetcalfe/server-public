package com.mimecast.server.services;

public interface EventBusService
{
    void post(Object event);
}
