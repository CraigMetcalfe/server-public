package com.mimecast.server.services;

import com.mimecast.server.data.models.SmsMessage;
import de.daslaboratorium.machinelearning.classifier.Classification;

import java.util.Collection;

public interface MessageClassifier
{
    /**
     *
     * @param message
     */
    void addTrainingMessage(final SmsMessage message);

    /**
     *
     * @param message
     * @return
     */
    Classification<String, String> getClassification(final String message);

    /**
     *
     * @param message
     * @return
     */
    Collection<Classification<String, String>> getDetailedClassification(final String message);

    /**
     *
     * @param message
     * @return
     */
    Classification<String, String> getCategoryClassification(final String message);
}
