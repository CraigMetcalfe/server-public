package com.mimecast.server.services.impl;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import com.mimecast.server.annotations.EventBusSubscriber;
import com.mimecast.server.data.dao.MessageQueryOptions;
import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.enums.MessageType;
import com.mimecast.server.data.messages.MessageAction;
import com.mimecast.server.data.models.SmsMessage;
import com.mimecast.server.services.MessageClassifier;
import com.mimecast.server.services.SmsMessageProcessor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@EventBusSubscriber
public class SmsMessageProcessorImpl
    implements SmsMessageProcessor
{
    @Autowired
    private MessageClassifier _messageClassifier;

    private List<SmsMessage> _smsMessages = new ArrayList<>();
    private Long _hamMessages = 0L;
    private Long _spamMessages = 0L;
    private Map<MessageCategory, Long> _categories = new HashMap<>();

    @Subscribe
    @AllowConcurrentEvents
    public void onSmsMessagePublish(MessageAction.NewSmsMessage action)
    {
        final SmsMessage message = action.getSmsMessage();
        _messageClassifier.addTrainingMessage(message);

        final MessageType messageType = message.getType();
        increaseTypeCount(messageType);

        if (MessageType.SPAM.equals(messageType))
        {
            final MessageCategory category = MessageCategory.fromString(
                    _messageClassifier.getCategoryClassification(message.getContents()).getCategory());
            message.setCategory(category);
            increaseCategoryCount(category);
        }
        _smsMessages.add(message);
    }

    @Override
    public List<SmsMessage> getMessages(final MessageQueryOptions options)
    {
        Stream<SmsMessage> messageStream = _smsMessages.stream();
        if (options.getMessageType() != null)
        {
            messageStream = messageStream.filter(message -> message.getType().equals(options.getMessageType()));
        }

        if (options.getMessageCategory() != null)
        {
            messageStream = messageStream.filter(message -> message.getCategory().equals(options.getMessageCategory()));
        }

        if (options.getOffset() != null)
        {
            messageStream = messageStream.skip(options.getOffset());
        }

        if (options.getLimit() != null)
        {
            messageStream = messageStream.limit(options.getLimit());
        }

        return messageStream.collect(Collectors.toList());
    }

    @Override
    public Pair<Long, Long> countHamSpam()
    {
        return new ImmutablePair<>(_hamMessages, _spamMessages);
    }

    @Override
    public Map<MessageCategory, Long> countCategories()
    {
        return _categories;
    }

    private void increaseTypeCount(final MessageType messageType)
    {
        if (MessageType.SPAM.equals(messageType))
        {
            _spamMessages++;
        }
        else
        {
            _hamMessages++;
        }
    }

    private void increaseCategoryCount(final MessageCategory messageCategory)
    {
        Long count = _categories.get(messageCategory);
        count = (count == null) ? 1 : count + 1;
        _categories.put(messageCategory, count);
    }
}
