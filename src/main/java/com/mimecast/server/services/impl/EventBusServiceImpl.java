package com.mimecast.server.services.impl;

import com.google.common.eventbus.EventBus;
import com.mimecast.server.annotations.EventBusSubscriber;
import com.mimecast.server.services.EventBusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service
public class EventBusServiceImpl
    implements EventBusService
{
    private final EventBus _eventBus = new EventBus();

    @Autowired
    private ApplicationContext _applicationContext;

    @PostConstruct
    public void registerListeners()
    {
        Map<String, Object> beans = _applicationContext.getBeansWithAnnotation(EventBusSubscriber.class);
        for (Object bean : beans.values())
        {
            _eventBus.register(bean);
        }
    }

    public void post(final Object event)
    {
        _eventBus.post(event);
    }
}
