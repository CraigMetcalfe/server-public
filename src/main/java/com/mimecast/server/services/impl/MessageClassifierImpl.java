package com.mimecast.server.services.impl;

import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.models.SmsMessage;
import com.mimecast.server.services.MessageClassifier;
import de.daslaboratorium.machinelearning.classifier.Classification;
import de.daslaboratorium.machinelearning.classifier.bayes.BayesClassifier;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;

@Service
public class MessageClassifierImpl
    implements MessageClassifier
{
    private static BayesClassifier<String, String> _spamClassifier = new BayesClassifier<>();
    private static BayesClassifier<String, String> _spamCategoryClassifier = new BayesClassifier<>();

    static
    {
        for (MessageCategory category : MessageCategory.values())
        {
            _spamCategoryClassifier.learn(new Classification<>(category.getKeywords(), category.toString()));
        }
    }

    @Override
    public void addTrainingMessage(final SmsMessage message)
    {
        // Train the classifier using the message type (ham or spam) and the keywords of the message
        _spamClassifier.learn(message.getType().toString(), Arrays.asList(message.getContents().split("\\s")));
    }

    @Override
    public Classification<String, String> getClassification(final String message)
    {
        return _spamClassifier.classify(Arrays.asList(message.split("\\s")));
    }

    @Override
    public Collection<Classification<String, String>> getDetailedClassification(final String message)
    {
        return _spamClassifier.classifyDetailed(Arrays.asList(message.split("\\s")));
    }

    @Override
    public Classification<String, String> getCategoryClassification(final String message)
    {
        return _spamCategoryClassifier.classify(Arrays.asList(message.split("\\s")));
    }
}
