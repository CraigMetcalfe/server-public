package com.mimecast.server.controllers;

import com.google.common.collect.ImmutableMap;
import com.mimecast.server.data.dao.MessageQueryOptions;
import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.enums.MessageType;
import com.mimecast.server.data.models.SmsMessage;
import com.mimecast.server.services.MessageClassifier;
import com.mimecast.server.services.SmsMessageProcessor;
import de.daslaboratorium.machinelearning.classifier.Classification;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/spam")
public class SpamController
{
    @Autowired
    private MessageClassifier _messageClassifier;
    @Autowired
    private SmsMessageProcessor _messageProcessor;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getSpam(
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "category", required = false) String category)
    {
        MessageQueryOptions options = new MessageQueryOptions()
                .offset(offset)
                .limit(limit)
                .messageType(MessageType.fromString(type))
                .messageCategory(MessageCategory.fromString(category));

        List<SmsMessage> filteredMessages = _messageProcessor.getMessages(options);
        List<SmsMessage> totalMessages = _messageProcessor.getMessages(new MessageQueryOptions());

        Map<String, Object> response = new HashMap<>();
        response.put("items", filteredMessages);
        response.put("filtered", filteredMessages.size());
        response.put("total", totalMessages.size());

        return response;
    }

    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getSummary()
    {
        Pair<Long, Long> hamSpamCounts = _messageProcessor.countHamSpam();

        Map<String, Object> response = new HashMap<>();

        List<Map<String, Object>> donutChart = new ArrayList<>();
        donutChart.add(ImmutableMap.of("label", MessageType.HAM.toString(), "value", hamSpamCounts.getLeft()));
        donutChart.add(ImmutableMap.of("label", MessageType.SPAM.toString(), "value", hamSpamCounts.getRight()));

        response.put("donut", donutChart);

        List<Map<String, Object>> barChart = new ArrayList<>();
        for (Map.Entry<MessageCategory, Long> entry : _messageProcessor.countCategories().entrySet())
        {
            barChart.add(ImmutableMap.of("label", entry.getKey().toString(), "value", entry.getValue()));
        }
        response.put("bar", barChart);

        return response;
    }

    @RequestMapping(value = "/analyse", method = RequestMethod.POST)
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> analyseSpam(
            @RequestBody Map<String, String> data)
    {
        final String message = data.get("message");

        Map<String, Object> response = new HashMap<>();

        Classification<String, String> classification = _messageClassifier.getClassification(message);
        response.put("message", message);
        response.put("type", classification.getCategory());
        response.put("probability", classification.getProbability());

        Classification<String, String> categoryClassification = _messageClassifier.getCategoryClassification(message);
        response.put("category", categoryClassification.getCategory());

        return response;
    }

    //private
}
