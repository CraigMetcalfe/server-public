package com.mimecast.server.data.messages;

import com.mimecast.server.data.models.SmsMessage;

public abstract class MessageAction
{
    protected SmsMessage _smsMessage;

    public MessageAction(SmsMessage message)
    {
        _smsMessage = message;
    }

    public SmsMessage getSmsMessage()
    {
        return _smsMessage;
    }

    /**
     * Signifies a new SMS Message has been created
     */
    public static class NewSmsMessage
            extends MessageAction
    {
        public NewSmsMessage(SmsMessage message)
        {
            super(message);
        }
    }
}
