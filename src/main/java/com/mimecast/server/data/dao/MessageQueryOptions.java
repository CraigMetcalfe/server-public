package com.mimecast.server.data.dao;

import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.enums.MessageType;

public class MessageQueryOptions
{
    private MessageType     _messageType;
    private MessageCategory _messageCategory;
    private Integer         _limit = Integer.MAX_VALUE;
    private Integer         _offset = 0;

    public MessageType getMessageType()
    {
        return _messageType;
    }

    public MessageQueryOptions messageType(final MessageType messageType)
    {
        if (messageType != null)
        {
            _messageType = messageType;
        }
        return this;
    }

    public MessageCategory getMessageCategory()
    {
        return _messageCategory;
    }

    public MessageQueryOptions messageCategory(final MessageCategory messageCategory)
    {
        if (messageCategory != null)
        {
            _messageCategory = messageCategory;
        }
        return this;
    }

    public Integer getLimit()
    {
        return _limit;
    }

    public MessageQueryOptions limit(final Integer limit)
    {
        if (limit != null)
        {
            _limit = limit;
        }
        return this;
    }

    public Integer getOffset()
    {
        return _offset;
    }

    public MessageQueryOptions offset(final Integer offset)
    {
        if (offset != null)
        {
            _offset = offset;
        }
        return this;
    }
}
