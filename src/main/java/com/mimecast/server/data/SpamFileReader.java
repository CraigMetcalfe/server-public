package com.mimecast.server.data;

import com.mimecast.server.data.enums.MessageType;
import com.mimecast.server.data.messages.MessageAction;
import com.mimecast.server.data.models.SmsMessage;
import com.mimecast.server.services.EventBusService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@Configuration
public class SpamFileReader
{
    private static final String SPAM_FILE_LOCATION = "classpath:SMSSpam";

    @Autowired
    private EventBusService _eventBusService;

    @PostConstruct
    public void init()
        throws IOException
    {
        try (final InputStream inputStream = new ClassPathResource(SPAM_FILE_LOCATION).getInputStream())
        {
            String contents = IOUtils.toString(inputStream, StandardCharsets.UTF_8).trim();
            List<String> messages = Arrays.asList(contents.split(StringUtils.LF));
            messages.forEach(this::processRawMessage);
        }
    }

    private void processRawMessage(final String data)
    {
        if (StringUtils.isEmpty(data))
        {
            return;
        }

        List<String> typeContentsList = Arrays.asList(data.split("\t"));
        if (CollectionUtils.isEmpty(typeContentsList) || (typeContentsList.size() != 2))
        {
            return;
        }

        SmsMessage message = new SmsMessage(MessageType.fromString(typeContentsList.get(0)), typeContentsList.get(1));
        _eventBusService.post(new MessageAction.NewSmsMessage(message));
    }
}
