package com.mimecast.server.data.models;

import com.mimecast.server.data.enums.MessageCategory;
import com.mimecast.server.data.enums.MessageType;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class SmsMessage
{
    private MessageType     _type;
    private String          _contents;
    private MessageCategory _category;

    public SmsMessage(final MessageType type, final String contents)
    {
        _type = type;
        _contents = contents;
    }

    public MessageType getType()
    {
        return _type;
    }

    public SmsMessage setType(MessageType type)
    {
        _type = type;
        return this;
    }

    public boolean isSpam()
    {
        return MessageType.SPAM.equals(_type);
    }

    public String getContents()
    {
        return _contents;
    }

    public SmsMessage setContents(String contents)
    {
        _contents = contents;
        return this;
    }

    public MessageCategory getCategory()
    {
        return _category;
    }

    public SmsMessage setCategory(MessageCategory category)
    {
        _category = category;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (!(o instanceof SmsMessage))
        {
            return false;
        }

        final SmsMessage that = (SmsMessage) o;
        return Objects.equals(_type, that._type) &&
               Objects.equals(_contents, that._contents) &&
               Objects.equals(_category, that._category);
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(_type)
                .append(_contents)
                .append(_category)
                .toHashCode();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("type", _type)
                .append("contents", _contents)
                .append("category", _category)
                .toString();
    }
}
