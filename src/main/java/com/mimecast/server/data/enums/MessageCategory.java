package com.mimecast.server.data.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public enum MessageCategory
{
    // https://arrow.dit.ie/cgi/viewcontent.cgi?article=1022&context=scschcomart
    RINGTONES("ringtones", Arrays.asList("send", "ringtone", "text", "tone", "free", "sms", "reply", "mobile")),
    CLAIMS("claims", Arrays.asList("accident", "entitled", "records", "pounds", "claim", "msg", "compensation", "opt")),
    COMPETITIONS("competitions", Arrays.asList("txt", "win", "uk", "voucher", "cash", "150p", "send", "entry")),
    PRIZES("prizes", Arrays.asList("prize", "guaranteed", "urgent", "todays", "valid", "claim", "draw", "cash")),
    VOICEMAIL("voicemail", Arrays.asList("please", "message", "voicemail", "waiting", "call", "delivery", "immediately", "urgent")),
    DATING("dating", Arrays.asList("dating", "service", "contacted", "find", "guess", "statement", "points", "private")),
    SERVICES("services", Arrays.asList("mins", "video", "free", "camera", "orange", "latest", "phone", "camcorder")),
    FINANCE("finance", Arrays.asList("help", "debt", "credit", "info", "government", "loans", "solution", "bills")),
    CHAT("chat", Arrays.asList("naughty", "ring", "alone", "chat", "xx", "heard", "luv", "home")),
    MISCELLANEOUS("miscellaneous", Arrays.asList(" find", "secret", "admirer", "special", "looking", "r*reveal", "contact", "call"));

    private static Map<String, MessageCategory> MAP = new HashMap<>();

    static
    {
        for (MessageCategory messageCategory : MessageCategory.values())
        {
            MAP.put(messageCategory.toString(), messageCategory);
        }
    }

    private final String       _value;
    private final List<String> _keywords;

    MessageCategory(String value, List<String> keywords)
    {
        _value = value;
        _keywords = keywords;
    }

    public static MessageCategory fromString(String value)
    {
        if (value == null)
        {
            return null;
        }

        MessageCategory type = MAP.get(value);
        if (type != null)
        {
            return type;
        }

        throw new IllegalArgumentException("No matching MessageCategory for: " + value);
    }

    public Set<String> getKeywords()
    {
        return new HashSet<>(_keywords);
    }

    @Override
    public String toString()
    {
        return _value;
    }
}
