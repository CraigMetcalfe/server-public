package com.mimecast.server.data.enums;

import java.util.HashMap;
import java.util.Map;

public enum MessageType
{
    SPAM("spam"),
    HAM("ham");

    private static Map<String, MessageType> MAP = new HashMap<>();

    static
    {
        for (MessageType messageType : MessageType.values())
        {
            MAP.put(messageType.toString(), messageType);
        }
    }

    private final String _value;

    MessageType(String value)
    {
        _value = value;
    }

    public static MessageType fromString(String value)
    {
        if (value == null)
        {
            return null;
        }

        MessageType type = MAP.get(value);
        if (type != null)
        {
            return type;
        }

        throw new IllegalArgumentException("No matching MessageType for: " + value);
    }

    @Override
    public String toString()
    {
        return _value;
    }
}
